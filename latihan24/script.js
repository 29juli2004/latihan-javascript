//membuat object
//object literal
var mhs1 = {
    nama:'sandhika galih',
    nrp:'043040023',
    email:'sandhikagalih@unpas.ac.id',
    jurusan:'Teknik informatika'
}

var mhs2 = {
    nama:'Doddy',
    nrp:'033040007',
    email:'doddy@unpas.com',
    jurusan:'Teknik industri'
}



//funchion declaration
functionbuatObjectMahasiswa(nama, nrp, email, jurusan); {
    var mhs = {};
    mhs.nama= nama;
    mhs.nrp=nrp;
    mhs.email=email;
    mhs.jurusan= jurusan;
    return mhs;
}

var mhs3 = buatObjectMahasiswa('Nofariza', '023040123','nofa@yahoo.com','teknik pangan');

//constructor
function Mahasiswa(nama, nrp, email, jurusan){
    //var this= {};
    this.nama = nama;
    this.nrp=nrp;
    this.email= email;
    this.jurusan = jurusan;
    //return this;
}
var mhs4 = Mahasiswa('Erik', '013040321','erik@icloud.com','Teknik mesin');